-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2016 at 03:32 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webtintuc`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `IDCategory` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameCategory` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`IDCategory`, `NameCategory`) VALUES
('CG_01', 'Xã hội'),
('CG_02', 'Giải trí'),
('CG_03', 'Thế giới'),
('CG_04', 'Học đường');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `IDComment` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IDNews` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UserName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Content` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`IDComment`, `IDNews`, `UserName`, `Date`, `Content`) VALUES
('CM_01', '', 'NguyenVanA', '2016-09-12', '');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `IDNew` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Summary` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UrlImage` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Date` date DEFAULT NULL,
  `IdUser` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Content` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IDCategory` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IDTag` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`IDNew`, `Title`, `Summary`, `UrlImage`, `Date`, `IdUser`, `Content`, `IDCategory`, `IDTag`) VALUES
('N_01', 'Để có bức ảnh "nghìn like", hãy chụp ảnh theo tông màu này', 'Đây là tông màu được yêu thích nhất thế giới, thế nên chỉ cần bức ảnh mang tông màu này thôi, bạn sẽ câu được cả "nghìn like" đấy!', '', '2016-09-10', 'US_01', 'Bạn cho rằng, sống ảo là một công việc cực dễ? Nếu vậy thì bạn lầm to rồi, bởi để có một bức ảnh "nghìn like" - chủ nhân của nó phải vật vã, mệt mỏi và tốn nhiều công sức set up rồi chụp, rồi chỉnh sửa qua hàng chục phần mềm nữa cơ. \r\n\r\nNhưng nếu bạn không có quá nhiều thời gian mà muốn công cuộc "câu like" của mình đạt được hiệu quả, thì hãy chụp những bức ảnh có tông màu sắp được bật mí dưới đây.\r\n\r\nĐó là tông màu gì? Xin thưa - chính là tông màu xanh da trời đấy!', 'CG_01', 'T_01'),
('N_02', '5 phát minh tưởng là hay nhưng... thật ra chẳng ra gì', 'Thỉnh thoảng lại có những ý tưởng mới mẻ ra đời nhưng lại không được nhiều người hưởng ứng cho lắm.', '', '2016-09-14', 'US_02', 'Cùng với sự phát triển không ngừng của khoa học công nghệ, nhiều phát minh liên tục ra đời thể hiện trình độ sáng tạo vô tận của con người.\r\n\r\nTuy nhiên, một số nhà phát minh lại quên rằng "có cung thì mới có cầu". Họ nghĩ ra những ý tưởng thật hay ho nhưng đến khi chúng được thực hiện thì ai nhìn vào cũng lắc đầu ngán ngẩm.\r\n\r\nNhững phát minh dưới đây chắc chắn cũng sẽ khiến bạn thốt lên rằng: "Trên Trái đất cũng có những thứ kỳ cục thế này hả trời?".', 'CG_02', 'T_02');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `IDRole` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `RoleName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`IDRole`, `RoleName`) VALUES
('R_01', 'Nguoi dung'),
('R_02', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `IDTag` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TagName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`IDTag`, `TagName`) VALUES
('T_01', 'aaa'),
('T_02', 'bbb');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UserName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateRegistee` date NOT NULL,
  `RoleID` varchar(59) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Birthday` date NOT NULL,
  `Gender` varchar(59) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Name`, `UserName`, `Password`, `Address`, `Phone`, `Email`, `DateRegistee`, `RoleID`, `Birthday`, `Gender`) VALUES
('US_02', 'Trần Thị Minh Tâm', 'Minh Tâm', '1234', 'Không biết', '123456789', 'abcd@gmail.com', '2016-09-14', 'R_02', '2016-09-21', 'Nữ'),
('US_1', 'Trần Thị Như Hiếu', 'Hiếu Trần', '1234', 'Không biết', '123456789', 'abcd@gmail.com', '2016-09-29', 'R_01', '2016-09-13', 'Nữ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`IDCategory`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`IDComment`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`IDNew`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`IDRole`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`IDTag`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
